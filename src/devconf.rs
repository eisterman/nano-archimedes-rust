/* ===============================================================
nano-archimedes-rust
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

use ndarray::{ArrayViewMut1,ArrayViewMut2,ArrayViewMut4};
use rand::Rng;

use crate::utils::rnd;
use crate::config::*;

use crate::annihilation::annihilation;

// initial configuration of electrons in device

pub fn devconf<T: Rng>(mut dist: ArrayViewMut4<i64>,
                       mut inum: &mut i64,
                       mut rng: &mut T,
                       mut px: ArrayViewMut1<f64>,
                       mut py: ArrayViewMut1<f64>,
                       mut kx: ArrayViewMut1<i64>,
                       mut ky: ArrayViewMut1<i64>,
                       mut w: ArrayViewMut1<i64>){
    // definition of a wave packet distribution function
    println!("\n\ndevconf() - calculation of the initial Wigner function..");
    for i in 1..=NX {
        for j in 1..=NY {
            for l in (-NKX+1)..NKX {
                for m in (-NKY+1)..NKY {
                    dist[[i,j,(l+NKX-1) as usize,(m+NKY-1) as usize]] =
                        ( *inum as f64 * (-(((i as f64-0.5)*DX-X0_WAVE_PACKET)/SIGMA_WAVE_PACKET).powi(2)
                            -(((j as f64-0.5)*DY-Y0_WAVE_PACKET)/SIGMA_WAVE_PACKET).powi(2)).exp()
                            *(-(((l as f64*DKX)-KX0_WAVE_PACKET)*SIGMA_WAVE_PACKET).powi(2)
                            -(((m as f64*DKY)-KY0_WAVE_PACKET)*SIGMA_WAVE_PACKET).powi(2)).exp()
                        ) as i64;
                }
            }
        }
    }
    println!("devconf() - calculation of the initial position of particles in phase space..");
    annihilation(dist.view_mut(),
            inum,
            rng,
            px.view_mut(),
            py.view_mut(),
            kx.view_mut(),
            ky.view_mut(),
            w.view_mut());

    println!("devconf() - Initial Number of Electron Super-particles = {}\n", inum);
}