/* ===============================================================
nano-archimedes-rust
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

use ndarray::{ArrayViewMut1,ArrayViewMut2,ArrayViewMut4};
use rand::Rng;

use crate::utils::rnd;
use crate::config::*;

// annihilates all unnecessary particles according to the
// previously calculated distribution function dist[(,,,)]

pub fn annihilation<T: Rng>(mut dist: ArrayViewMut4<i64>,
                            mut inum: &mut i64,
                            mut rng: &mut T,
                            mut px: ArrayViewMut1<f64>,
                            mut py: ArrayViewMut1<f64>,
                            mut kx: ArrayViewMut1<i64>,
                            mut ky: ArrayViewMut1<i64>,
                            mut w: ArrayViewMut1<i64>){
    println!("\n# of particles before annihilation = {}", inum);
    // calculates the new array of particles
    *inum = 0;
    for i in 1..=NX {
        for j in 1..=NY {
            for ikx in 0..(2*NKX-1) {
                for jky in 0..(2*NKY-1) {
                    let local_number_of_particles: i64 = dist[[i,j,ikx as usize, jky as usize]].abs();
                    // creates the new local particles in the (i,j,ikx,jky)-th phase-space cell
                    // the particles are uniformely distributed in space
                    for n in 1..=local_number_of_particles {
                        let m = (*inum + n) as usize;
                        if rnd(&mut rng) > 0.5 {
                            px[m] = (i as f64 - 0.5 + 0.5*rnd(&mut rng))*DX;
                        } else {
                            px[m] = (i as f64 - 0.5 - 0.5*rnd(&mut rng))*DX;
                        }
                        if rnd(&mut rng) > 0.5 {
                            py[m] = (j as f64 - 0.5 + 0.5*rnd(&mut rng))*DY;
                        } else {
                            py[m] = (j as f64 - 0.5 - 0.5*rnd(&mut rng))*DY;
                        }
                        kx[m] = (ikx - NKX) as i64 + 1;
                        ky[m] = (jky - NKY) as i64 + 1;
                        if dist[[i,j,ikx as usize,jky as usize]] > 0 {
                            w[m] = 1;
                        } else {
                            w[m] = -1;
                        }
                    }
                    *inum += local_number_of_particles;
                }
            }
        }
    }
    println!("# of particles after annihilation  = {}\n", inum);
}