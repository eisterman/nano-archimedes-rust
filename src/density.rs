/* ===============================================================
nano-archimedes-rust
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

use ndarray::{ArrayViewMut1,ArrayViewMut2,ArrayViewMut4};
use ndarray::s;

use crate::config::*;

// calculate the density of particles in function of the position

pub fn density(mut rho: ArrayViewMut2<f64>,
               mut psi: ArrayViewMut2<f64>,
               inum: i64,
               mut px: ArrayViewMut1<f64>,
               mut py: ArrayViewMut1<f64>,
               mut kx: ArrayViewMut1<i64>,
               mut ky:ArrayViewMut1<i64>,
               mut w: ArrayViewMut1<i64>) {
    // reset the density array
    rho.fill(0.);
    psi.slice_mut(s!(0..(2*NKX-1),0..(2*NKY-1))).fill(0.);

    // calculation of the real space density
    println!("calculation of the real space density.");
    for n in 0..(inum as usize) {
        let i = (px[n]/DX + 1.) as i64;
        let j = (py[n]/DY + 1.) as i64;
        let ikx = kx[n];
        let jky = ky[n];
        if i > 0 && i <= NX as i64 &&
            j > 0 && j <= NY as i64 &&
            ((-NKX as i64) < ikx && ikx < (NKX as i64) ) &&
            ((-NKY as i64) < jky && jky < (NKY as i64) ) {
            rho[[i as usize, j as usize]] += w[n] as f64;
        }
    }

    // calculation of the phase space density
    println!("calculation of the phase space density.");
    for n in 0_usize..(inum as usize) {
        let i = (px[n]/DX + 1.) as i64;
        let j = (py[n]/DY + 1.) as i64;
        let ikx = kx[n];
        let jky = ky[n];
        if i > 0 && i <= NX as i64 &&
            j > 0 && j <= NY as i64 &&
            ((-NKX as i64) < ikx && ikx < (NKX as i64) ) &&
            ((-NKY as i64) < jky && jky < (NKY as i64) ) {
            psi[[(NKX as i64-1+ikx) as usize, (NKY as i64-1+jky) as usize]] += w[n] as f64;
        }
    }
}