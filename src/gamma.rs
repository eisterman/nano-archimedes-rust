/* ===============================================================
nano-archimedes-rust
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

use ndarray::{ArrayViewMut2,ArrayView4};

use crate::config::*;

// calculates gamma(x,y), i.e the number of couples created per unit of time

pub fn calculate_gamma(mut gamma: ArrayViewMut2<f64>,
                       vw: ArrayView4<f64>) {
    for i in 1..=NX {
        for j in 1..=NY {
            gamma[(i,j)] = 0.;
            // Note that in 2D the Wigner potential is NOT anti-symmetric w.r.t. the k-space.
            // (see Sellier manuscript)
            for ikx in 0..(2*(NKX as usize)) {
                for jky in 0..(2*(NKY as usize)) {
                    // use vm+
                    if vw[(i,j,ikx,jky)] > 0. {
                        gamma[(i,j)] += vw[(i,j,ikx,jky)]
                    }
                }
            }
        }
    }
}