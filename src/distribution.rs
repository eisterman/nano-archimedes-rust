/* ===============================================================
nano-archimedes-rust
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

use ndarray::{ArrayView1,ArrayViewMut4};

use crate::config::*;

// calculates the distribution function according to the quantum weights

pub fn distribution(inum: i64,
                    mut dist: ArrayViewMut4<i64>,
                    px: ArrayView1<f64>,
                    py: ArrayView1<f64>,
                    kx: ArrayView1<i64>,
                    ky: ArrayView1<i64>,
                    w: ArrayView1<i64>) {
    println!("\nCalculation of distribution function");
    println!("Number of particles = {}", inum);

    // set the distribution function to zero
    dist.fill(0);

    for n in 0..inum as usize {
        let i = (px[n]/DX) as i64 + 1;
        let j = (py[n]/DY) as i64 + 1;
        let ikx = kx[n];
        let jky = ky[n];
        if 0 < i && i <= NX as i64 && (-NKX as i64) < ikx && ikx < (NKX as i64) &&
            0 < j && j <= NY as i64 && (-NKY as i64) < jky && jky < (NKY as i64) {
            dist[(i as usize,j as usize,(ikx+(NKX as i64)-1) as usize,(jky+(NKY as i64)-1) as usize)] += w[n];
        }
    }

    println!("end of distribution function calculation");
}