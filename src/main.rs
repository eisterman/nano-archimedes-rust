/* ===============================================================
nano-archimedes-rust
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

use rand::prelude::*;
use ndarray::{Array1,Array2,Array4};
use ndarray::s;
use chrono::{DateTime, Local};
use std::fs::create_dir_all;

mod utils;
use utils::rnd;

mod devconf;
use devconf::devconf;

mod annihilation;
use annihilation::annihilation;

mod density;
use density::density;

mod save;
use save::save;

mod wigner;
use wigner::wigner;

mod gamma;
use gamma::calculate_gamma;

mod emc;
use emc::emc;

mod distribution;
use distribution::distribution;

const NPMAX: usize = 950000000; // maximum number of super-particles

mod config;
use config::*;

fn main() {
    let mut inum: i64 = 512;    // define initial number of particles

    // define random numbers generator
    const ISEED: u64 = 38467;
    let mut rng = rand::rngs::StdRng::seed_from_u64(ISEED);

    // print the initial number of particles
    print!("\nMAXIMUM_NUMBER_OF_PARTICLES_ALLOWED = {}\n\n", NPMAX);

    // memory allocation
    let shape4 = (NX + 1, NY + 1, 2 * NKX as usize + 1, 2 * NKY as usize + 1);
    let mut vw = Array4::<f64>::zeros(shape4);
    let mut dist = Array4::<i64>::zeros(shape4);
    let mut phi = Array2::<f64>::zeros((NX + 1, NY + 1));
    let mut rho = Array2::<f64>::zeros((NX + 1, NY + 1));
    let mut gamma = Array2::<f64>::zeros((NX + 1, NY + 1));
    let mut psi = Array2::<f64>::zeros((2 * NKX as usize + 1, 2 * NKY as usize + 1));
    let mut kx = Array1::<i64>::zeros(NPMAX + 1);
    let mut ky = Array1::<i64>::zeros(NPMAX + 1);
    let mut w = Array1::<i64>::zeros(NPMAX + 1);
    let mut updated = Array1::<bool>::from_elem(NPMAX + 1, false);
    let mut px = Array1::<f64>::zeros(NPMAX + 1);
    let mut py = Array1::<f64>::zeros(NPMAX + 1);
    let mut ptime = Array1::<f64>::zeros(NPMAX + 1);
    println!("All memory has been allocated successfully.\n");

    // definition of a barrier on the upper half part of the domain
    println!("creating the potential profile..");
    // horizontal wall
    phi.slice_mut(s![((65.0e-9 / DX + 0.5) as usize)..,1..]).fill(VBARRIER);
//    for i in 1..=NX {
//        for j in 1..=NY {
//            let x = (i as f64 - 0.5) * DX;
//            let _y = (j as f64 - 0.5) * DY;
//            //phi[(i,j)] = 0.;
//            if x >= 65.0e-9 {
//                phi[[i,j]] = VBARRIER;
//            }
//        }
//    }
    println!("potential profile created.\n");

    // gamma function already setted to zero

    // get initial time
    let now: DateTime<Local> = Local::now();

    // initializations
    devconf(dist.view_mut(),
            &mut inum,
            &mut rng,
            px.view_mut(),
            py.view_mut(),
            kx.view_mut(),
            ky.view_mut(),
            w.view_mut());               // device configuration - distributes particles in the device
    density(rho.view_mut(),
            psi.view_mut(),
            inum,
            px.view_mut(),
            py.view_mut(),
            kx.view_mut(),
            ky.view_mut(),
            w.view_mut());
    create_dir_all("out").expect("out folder error"); // ensure out folder exist
    save(0,
         vw.view(),
         phi.view(),
         gamma.view(),
         dist.view(),
         rho.view(),
         psi.view()).expect("Saving Error!");

    print!("\n");
    let mut time = 0.;
    // updates the solution
    for i in 1..=ITMAX {
        time += DT;
        println!("{0} of {1} -- TIME={2}\n", i, ITMAX, time);
        if i == 1 {
            println!("Calculating Wigner potential");
            wigner(vw.view_mut(), phi.view());
            println!("Calculating Gamma function");
            calculate_gamma(gamma.view_mut(), vw.view());
        }
        println!("Evolving particles");
        emc(updated.view_mut(),
            ptime.view_mut(),
            &mut inum,
            px.view_mut(),
            py.view_mut(),
            kx.view_mut(),
            ky.view_mut(),
            gamma.view(),
            &mut rng,
            vw.view(),
            w.view_mut());
        println!("Calculating distribution function");
        distribution(inum,
                     dist.view_mut(),
                     px.view(),
                     py.view(),
                     kx.view(),
                     ky.view(),
                     w.view());
        println!("Calculating particle density");
        density(rho.view_mut(),
                psi.view_mut(),
                inum,
                px.view_mut(),
                py.view_mut(),
                kx.view_mut(),
                ky.view_mut(),
                w.view_mut());
        println!("Annihilation of particles");
        if i != ITMAX {
            annihilation(dist.view_mut(),
                         &mut inum,
                         &mut rng,
                         px.view_mut(),
                         py.view_mut(),
                         kx.view_mut(),
                         ky.view_mut(),
                         w.view_mut());
        }
        // eventually Poisson should go here
        save(i,
             vw.view(),
             phi.view(),
             gamma.view(),
             dist.view(),
             rho.view(),
             psi.view()).expect("Saving Error!"); // save output every time step
    }
    print!("\n");

    println!("output files saved\n");

    // get final time and exit.
    println!("simulation started   : {}", now);
    let now: DateTime<Local> = Local::now();
    println!("simulation ended     : {}",now);
}