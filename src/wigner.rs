/* ===============================================================
nano-archimedes-rust
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

use std::io::{self,Write};

use ndarray::{ArrayView2,ArrayViewMut4};

use crate::config::*;

// calculates the Wigner kernel/potential

// These calculations are useful to calculate the function gamma.

pub fn wigner(mut vw: ArrayViewMut4<f64>,
              phi: ArrayView2<f64>) {
    for i in 1..=NX {
        print!("{0}/{1} - ", i, NX);
        io::stdout().flush().unwrap();
        for j in 1..=NY {
            for ikx in 0..(2*NKX) {
                for jky in 0..(2*NKY) {
                    vw[(i,j,ikx as usize,jky as usize)] = 0.;
                    for l in 1..=((0.5*LC/DX) as i64 +1) {
                        for m in (-(LC/DY) as i64 -1)..=((LC/DY) as i64 +1) {
                            let i = i as i64;
                            let j = j as i64;
                            let ikx = ikx as i64;
                            let jky = jky as i64;
                            if 1 <= (i+l) && (i+l) <= (NX as i64) &&
                                1 <= (i-l) && (i-l) <= (NX as i64) &&
                                1 <= (j+m) && (j+m) <= (NY as i64) &&
                                1 <= (j-m) && (j-m) <= (NY as i64) {
                                vw[(i as usize, j as usize, ikx as usize, jky as usize)] += (2.
                                    * (ikx - NKX as i64 + 1) as f64 * DKX * ((l as f64) - 0.5) * DX
                                    + 2. * ((jky - NKY as i64 + 1) as f64) * DKY * ((m as f64) - 0.5)
                                    * DY).sin() * (phi[((i + l) as usize, (j + m) as usize)]
                                    - phi[((i - l) as usize, (j - m) as usize)]);
                            }
                        }
                    }
                    vw[(i,j,ikx as usize,jky as usize)] *= -2.*(-Q)*DX*DY/(HBAR*LC*LC);
                }
            }
        }
    }
    print!("\n");
}