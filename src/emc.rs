/* ===============================================================
nano-archimedes-rust
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

use ndarray::{ArrayViewMut1, ArrayView2, ArrayView4};
use ndarray::s;
use rand::Rng;

use std::process::exit;

use crate::utils::rnd;

use crate::config::*;
use crate::NPMAX;

// ensemble Monte Carlo method

pub fn emc<T: Rng>(mut updated: ArrayViewMut1<bool>,
                   mut ptime: ArrayViewMut1<f64>,
                   mut inum: &mut i64,
                   mut px: ArrayViewMut1<f64>,
                   mut py: ArrayViewMut1<f64>,
                   mut kx: ArrayViewMut1<i64>,
                   mut ky: ArrayViewMut1<i64>,
                   gamma: ArrayView2<f64>,
                   mut rng: &mut T,
                   vw: ArrayView4<f64>,
                   mut w: ArrayViewMut1<i64>) {
    // evolution of the particles
    // and creation of (+,-) couples

    // initial settings
    let mut number_of_outside_particles = 0;
    let mut all_particles_updated = false;
    updated.slice_mut(s![0..(*inum as usize)]).fill(false);
    ptime.slice_mut(s![0..(*inum as usize)]).fill(DT);

    while all_particles_updated == false {
        let mut number_of_created_particles = 0;
        // evolution and couples creation
        for n in 0..(*inum as usize) {
            if updated[n] == false {
                let hmt = HBAR/(MSTAR*M)*ptime[n];
                // drift n-th particle
                let x0 = px[n];
                let y0 = py[n];
                let kx0 = kx[n] as f64*DKX;
                let ky0 = ky[n] as f64*DKY;
                let i = (x0/DX + 1.) as usize;
                let j = (y0/DY + 1.) as usize;
                // evolve position and wave vector of the n-th particle
                if i > 0 && i as i64 <= NX as i64 && (-NKX as i64) < kx[n] && kx[n] < NKX as i64 &&
                    j > 0 && j as i64 <= NY as i64 && (-NKY as i64) < ky[n] && ky[n] < NKY as i64{
                    px[n] = x0 + hmt * kx0;
                    py[n] = y0 + hmt * ky0;
                    // calculates the probability that the wave-vector
                    // actually evolves according to the continuous dkx
                    // check if a couple of (+,-) have to be created
                    if gamma[(i,j)] != 0. {
                        let mut time = 0.;
                        while time < ptime[n] {
                            let rdt = - rnd(&mut rng).ln()/gamma[(i,j)];
                            time += rdt;
                            if time < ptime[n] {
                                let mut created = false;
                                let r = rnd(&mut rng);
                                let mut sum = 0.;
                                // random selection of the wave-vector
                                'outer: for ikx in (-NKX+1)..NKX {
                                    if created {
                                        break;
                                    }
                                    for jky in (-NKY+1)..NKY {
                                        if created {
                                            break 'outer;
                                        }
                                        let p = {
                                            if vw[(i,j,(ikx+NKX-1) as usize,(jky+NKY-1) as usize)] > 0. {
                                                vw[(i,j,(ikx+NKX-1) as usize,(jky+NKY-1) as usize)]/gamma[(i,j)]
                                            } else {
                                                0.
                                            }
                                        };
                                        //println!("n = {0} --- sum = {1}  ---  p = {2}",n, sum, p);
                                        //if n != 0 { return }
                                        if sum <= r && r < (sum+p) {
                                            number_of_created_particles += 2;
                                            let mut num = (*inum + number_of_created_particles) as usize;
                                            // select a random time interval when the creation happens
                                            // assign position
                                            px.slice_mut(s![(num-2..=num-1)]).fill(x0+HBAR/(MSTAR*M)*time*kx0);
                                            py.slice_mut(s![(num-2..=num-1)]).fill(y0+HBAR/(MSTAR*M)*time*ky0);
                                            // assign wave-vector
                                            if vw[(i,j,(ikx+NKX-1) as usize,(jky+NKY-1) as usize)] >= 0. {
                                                kx[num-2] = kx[n] + ikx as i64;
                                                kx[num-1] = kx[n] - ikx as i64;
                                                ky[num-2] = ky[n] + jky as i64;
                                                ky[num-1] = ky[n] - jky as i64;
                                            } else {
                                                kx[num-2] = kx[n] - ikx as i64;
                                                kx[num-1] = kx[n] + ikx as i64;
                                                ky[num-2] = ky[n] - jky as i64;
                                                ky[num-1] = ky[n] + jky as i64;
                                            }
                                            // assign quantum weight
                                            if w[n] == 1 {
                                                w[num-2] = 1;
                                                w[num-1] = -1;
                                            } else {
                                                w[num-2] = -1;
                                                w[num-1] = 1;
                                            }
                                            // assign flag to evolve the particles at the next loop
                                            updated.slice_mut(s![(num-2..=num-1)]).fill(false);
                                            // assign time
                                            let ptime_n = ptime[n];
                                            ptime.slice_mut(s![num-2..=num-1]).fill(ptime_n - time);
                                            // eventually ignore the just-created couple since
                                            // at least one of them is outside the device
                                            if kx[num-2] <= -NKX as i64 || kx[num-2] >= NKX as i64 ||
                                                kx[num-1] <= -NKX as i64 || kx[num-1] >= NKX as i64 ||
                                                ky[num-2] <= -NKY as i64 || ky[num-2] >= NKY as i64 ||
                                                ky[num-1] <= -NKY as i64 || ky[num-1] >= NKY as i64 {
                                                num = (*inum-2) as usize;
                                                number_of_created_particles -= 2;
                                            }
                                            created = true;
                                        }
                                        sum += p;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    number_of_outside_particles += 1;
                }
                updated[n] = true;
            }
        }
        *inum += number_of_created_particles;
        println!("INUM = {0} -- particles created = {1}", inum, number_of_created_particles);
        if *inum > NPMAX as i64 {
            println!("Number of particles has exploded - please increase NPMAX and recompile");
            exit(1);
        }
        // checks if all particles have been updated
        all_particles_updated = updated.slice(s!(0..(*inum as usize))).iter().all(|x| { *x });
    }
    println!("-- number of particles outside = {} --", number_of_outside_particles);
}