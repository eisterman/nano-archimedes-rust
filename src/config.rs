/* ===============================================================
nano-archimedes-rust
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

// definition of constants
pub const Q: f64 = 1.60217733e-19;    // Electron charge in absolute value (Coulomb)
pub const HBAR: f64 = 1.05457266e-34; // Reduced Planck constant (Joule*sec)
pub const M: f64 = 9.1093897e-31;     // free electron mass (Kg)
pub use std::f64::consts::PI;  // Pi number
pub const MSTAR: f64 = 0.067;         // Nedjalkov

// define device properties
pub const LX: f64 = 150.0e-9;     // x-direction total lenght of device
pub const LY: f64 = 150.0e-9;     // y-direction total lenght of device
pub const LC: f64 = 100.0e-9;      // coherence lenght
pub const NX: usize = 150;          // number of cells in x-direction
pub const NY: usize = 100;          // number of cells in y-direction
pub const NKX: isize = 32;         // number of cells in kx-direction
pub const NKY: isize = 32;         // number of cells in ky-direction
pub const DT: f64 = 1.0e-15;      // time step
pub const ITMAX: i64 = 300;      // total number of time steps

 // against the wall in oblique direction
pub const SIGMA_WAVE_PACKET: f64 = 10.0e-9; // wave packet width
pub const X0_WAVE_PACKET: f64 = 50.0e-9;    // wave packet initial position in x-direction
pub const Y0_WAVE_PACKET: f64 = 0.4 * LY;    // wave packet initial position in y-direction
pub const KX0_WAVE_PACKET: f64 = 2.5e8;    // wave packet initial Kx wave vector
pub const KY0_WAVE_PACKET: f64 = 2.5e8;    // wave packet initial Ky wave vector

pub const VBARRIER: f64 = -0.05;

// spatial cell lenght
pub const DX: f64 = LX / (NX as f64);
pub const DY: f64 = LY / (NY as f64);

// pseudo-wave vector lenght
pub const DKX: f64 = PI / LC;
pub const DKY: f64 = PI / LC;