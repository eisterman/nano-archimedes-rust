/* ===============================================================
nano-archimedes-rust
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

use std::fs::File;
use std::io::prelude::*;
use std::fmt::write;

use ndarray::{ArrayView2, ArrayView4};

use crate::config::*;

pub fn save(ind: i64, vw: ArrayView4<f64>,
            phi: ArrayView2<f64>,
            gamma: ArrayView2<f64>,
            _dist: ArrayView4<i64>,
            rho: ArrayView2<f64>,
            psi: ArrayView2<f64>) -> std::io::Result<()>{
    println!("\nsaving output files = {}..", ind);

    // save Wigner potential on 5 points
    let mut fp = File::create("out/wigner_70x75.dat")?;
    for i in 0_usize..(2*NKX as usize) {
        for j in 0_usize..(2*NKY as usize) {
            write!(fp, "{} ", vw[((70.0e-9/DX) as usize, (NY/2) as usize, i, j)])?;
        }
        write!(fp, "\n")?;
    }

    fp = File::create("out/wigner_70x37.dat")?;
    for i in 0_usize..(2*NKX as usize) {
        for j in 0_usize..(2*NKY as usize) {
            write!(fp, "{} ", vw[((70.0e-9/DX) as usize, (NY/4) as usize, i, j)])?;
        }
        write!(fp, "\n")?;
    }

    fp = File::create("out/wigner_70x2.dat")?;
    for i in 0_usize..(2*NKX as usize) {
        for j in 0_usize..(2*NKY as usize) {
            write!(fp, "{} ", vw[((70.0e-9/DX) as usize, 2, i, j)])?;
        }
        write!(fp, "\n")?;
    }

    fp = File::create("out/wigner_70x148.dat")?;
    for i in 0_usize..(2*NKX as usize) {
        for j in 0_usize..(2*NKY as usize) {
            write!(fp, "{} ", vw[((70.0e-9/DX) as usize, NY as usize - 2, i, j)])?;
        }
        write!(fp, "\n")?;
    }

    fp = File::create("out/wigner_70x108.dat")?;
    for i in 0_usize..(2*NKX as usize) {
        for j in 0_usize..(2*NKY as usize) {
            write!(fp, "{} ", vw[((70.0e-9/DX) as usize, NY*0.75 as usize, i, j)])?;
        }
        write!(fp, "\n")?;
    }

    // saves potential
    fp = File::create("out/potential.dat")?;
    for i in 1_usize..=(NX as usize) {
        for j in 1_usize..=(NY as usize) {
            write!(fp, "{} ", phi[(i, j)])?;
        }
        write!(fp, "\n")?;
    }

    // saves gamma function
    fp = File::create("out/gamma.dat")?;
    for i in 1_usize..=(NX as usize) {
        for j in 1_usize..=(NY as usize) {
            write!(fp, "{} ", gamma[(i, j)])?;
        }
        write!(fp, "\n")?;
    }

    /*
    //save distribution in some chosen (kx,ky) points
    fp = File::create(format!("out/distribution_{}.dat", ind))?;
    for i in 1_usize..=(NX as usize) {
        for j in (-NKX+1)..(NKX) {
            write!(fp, "{} ", dist[(i, (j+NKX-1) as usize)])?;
        }
        write!(fp, "\n")?;
    }
    */

    // saves the electron density
    fp = File::create(format!("out/dens_{0}_{1}.dat", LC*1.0e9, ind))?;
    for i in 1_usize..=(NX as usize) {
        for j in 1_usize..=(NY as usize) {
            write!(fp, "{} ", rho[(i, j)])?;
        }
        write!(fp, "\n")?;
    }

    // saves the phase-space density
    fp = File::create(format!("out/f_x_{0}_{1}.dat", LC*1.0e9, ind))?;
    for i in (-NKX+1)..NKX {
        for j in (-NKY+1)..NKY {
            write!(fp, "{} ", psi[((i+NKX-1) as usize, (j+NKY-1) as usize)])?;
        }
        write!(fp, "\n")?;
    }

    println!("output files saved successfully.\n\n");

    Ok(())
}